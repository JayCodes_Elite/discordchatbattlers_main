﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// include
using Newtonsoft.Json;


namespace ChatBattlers
{   
    
    class Entity
    {
        // --- PRIVATE MEMBERS
        [JsonProperty]
        private string _player;
        [JsonProperty]
        private string _name;
        [JsonProperty]
        private ulong _id;
        
        [JsonProperty]
        private int _techLvl; // Tech level represents over all strength of the entitu

        [JsonProperty]
        private int _wins;
        [JsonProperty]
        private int _losses;
        [JsonProperty]
        private string _type;

        // CORE ATTRIBUTES - - - - - - - -
        [JsonIgnore]
        public Attribute Off; // Offense, damage bonus
        [JsonIgnore]
        public Attribute Def; // Deffense, hp, damage reduction
        [JsonIgnore]
        public Attribute Eff; // Effeciany, evade / initiative
        [JsonIgnore]
        public Attribute Acc; // Accuaracy, crit chance, to hit chance

        // DERIVED ATTRIBUTES - - - - - - -
        [JsonIgnore]
        public Attribute Integrity; // How much damage it can take before destruction
        [JsonIgnore]
        public Attribute DamageBonus; // Damage Bonus applied to damage dealt
        [JsonIgnore]
        public Attribute DamageResist; // Amount to reduce damage by when taking damage
        [JsonIgnore]
        public Attribute HitChance; // Chance for a attack to hit, on dealing damage
        [JsonIgnore]
        public Attribute CritChance; // Chance for a crit hit, on dealing damage
        [JsonIgnore]
        public Attribute EvadeChance; // Chance to evade a attack all together
        [JsonIgnore]
        public Attribute ActionPriority; // turn order

        // Container to allow mass updates of the independent objects.
        // Need to assure this works as I think....
        [JsonIgnore]
        public Dictionary<LABEL, Attribute> AttributeDict;
        public List<Gear> AttachedGear;

        // --- CONSTRUCTORS
        public Entity()
        {
            // On load Entity from JSON
            Console.WriteLine("Loading entity " + _name + " from JSON, in constructor");
            // init blank attributes
            Off = new Attribute();
            Def = new Attribute();
            Eff = new Attribute();
            Acc = new Attribute();
            Integrity = new Attribute();
            DamageBonus = new Attribute();
            DamageResist = new Attribute();
            HitChance = new Attribute();
            CritChance = new Attribute();
            EvadeChance = new Attribute();
            ActionPriority = new Attribute();
            // init dict
            AttributeDict = new Dictionary<LABEL, Attribute>();
            AttachedGear = new List<Gear>();
        }
       
        public Entity(ulong id, string player, string name, string type)
        {
            // setup base attritbutes of bot, for now init all to just 10
            _name = name;
            _player = player;
            _id = id;
            _type = type;
            _wins = 0;
            _losses = 0;
            AttachedGear = new List<Gear>();            
            AttributeDict = new Dictionary<LABEL, Attribute>();

            // Based on type input assign defualt attribute values
            switch (_type)
            {
                case "SCOUT":
                    AttributeDict.Add(LABEL.OFF , Off = new Attribute(LABEL.OFF, 25, this));
                    AttributeDict.Add(LABEL.DEF, Def = new Attribute(LABEL.DEF, 10, this));
                    AttributeDict.Add(LABEL.EFF, Eff = new Attribute(LABEL.EFF, 25, this));
                    AttributeDict.Add(LABEL.ACC, Acc = new Attribute(LABEL.ACC, 20, this));
                    AttributeDict.Add(LABEL.INTEG, Integrity = new Attribute(LABEL.INTEG, 25, this));
                    AttributeDict.Add(LABEL.DMGBNS, DamageBonus = new Attribute(LABEL.DMGBNS, 15, this));
                    AttributeDict.Add(LABEL.DMGRES, DamageResist = new Attribute(LABEL.DMGRES, 5, this));
                    AttributeDict.Add(LABEL.HITCHN, HitChance = new Attribute(LABEL.HITCHN, 65, this));
                    AttributeDict.Add(LABEL.CRICHN, CritChance = new Attribute(LABEL.CRICHN, 4, this));
                    AttributeDict.Add(LABEL.EVACHN, EvadeChance = new Attribute(LABEL.EVACHN, 8, this));
                    AttributeDict.Add(LABEL.ACTPRI, ActionPriority = new Attribute(LABEL.ACTPRI, 10, this));
                    break;
                case "ASSAULT":                    
                    AttributeDict.Add(LABEL.OFF, Off = new Attribute(LABEL.OFF, 20, this));
                    AttributeDict.Add(LABEL.DEF, Def = new Attribute(LABEL.DEF, 20, this));
                    AttributeDict.Add(LABEL.EFF, Eff = new Attribute(LABEL.EFF, 15, this));
                    AttributeDict.Add(LABEL.ACC, Acc = new Attribute(LABEL.ACC, 25, this));
                    AttributeDict.Add(LABEL.INTEG, Integrity = new Attribute(LABEL.INTEG, 50, this));
                    AttributeDict.Add(LABEL.DMGBNS, DamageBonus = new Attribute(LABEL.DMGBNS, 10, this));
                    AttributeDict.Add(LABEL.DMGRES, DamageResist = new Attribute(LABEL.DMGRES, 10, this));
                    AttributeDict.Add(LABEL.HITCHN, HitChance = new Attribute(LABEL.HITCHN, 60, this));
                    AttributeDict.Add(LABEL.CRICHN, CritChance = new Attribute(LABEL.CRICHN, 8, this));
                    AttributeDict.Add(LABEL.EVACHN, EvadeChance = new Attribute(LABEL.EVACHN, 4, this));
                    AttributeDict.Add(LABEL.ACTPRI, ActionPriority = new Attribute(LABEL.ACTPRI, 10, this));
                    break;
                case "HEAVY":
                    AttributeDict.Add(LABEL.OFF, Off = new Attribute(LABEL.OFF, 15, this));
                    AttributeDict.Add(LABEL.DEF, Def = new Attribute(LABEL.DEF, 35, this));
                    AttributeDict.Add(LABEL.EFF, Eff = new Attribute(LABEL.EFF, 20, this));
                    AttributeDict.Add(LABEL.ACC, Acc = new Attribute(LABEL.ACC, 10, this));
                    AttributeDict.Add(LABEL.INTEG, Integrity = new Attribute(LABEL.INTEG, 75, this));
                    AttributeDict.Add(LABEL.DMGBNS, DamageBonus = new Attribute(LABEL.DMGBNS, 5, this));
                    AttributeDict.Add(LABEL.DMGRES, DamageResist = new Attribute(LABEL.DMGRES, 15, this));
                    AttributeDict.Add(LABEL.HITCHN, HitChance = new Attribute(LABEL.HITCHN, 50, this));
                    AttributeDict.Add(LABEL.CRICHN, CritChance = new Attribute(LABEL.CRICHN, 2, this));
                    AttributeDict.Add(LABEL.EVACHN, EvadeChance = new Attribute(LABEL.EVACHN, 2, this));
                    AttributeDict.Add(LABEL.ACTPRI, ActionPriority = new Attribute(LABEL.ACTPRI, 10, this));
                    break;
                default:
                    AttributeDict.Add(LABEL.OFF, Off = new Attribute(LABEL.OFF, 20, this));
                    AttributeDict.Add(LABEL.DEF, Def = new Attribute(LABEL.DEF, 20, this));
                    AttributeDict.Add(LABEL.EFF, Eff = new Attribute(LABEL.EFF, 20, this));
                    AttributeDict.Add(LABEL.ACC, Acc = new Attribute(LABEL.ACC, 20, this));
                    AttributeDict.Add(LABEL.INTEG, Integrity = new Attribute(LABEL.INTEG, 50, this));
                    AttributeDict.Add(LABEL.DMGBNS, DamageBonus = new Attribute(LABEL.DMGBNS, 5, this));
                    AttributeDict.Add(LABEL.DMGRES, DamageResist = new Attribute(LABEL.DMGRES, 2, this));
                    AttributeDict.Add(LABEL.HITCHN, HitChance = new Attribute(LABEL.HITCHN, 50, this));
                    AttributeDict.Add(LABEL.CRICHN, CritChance = new Attribute(LABEL.CRICHN, 2, this));
                    AttributeDict.Add(LABEL.EVACHN, EvadeChance = new Attribute(LABEL.EVACHN, 2, this));
                    AttributeDict.Add(LABEL.ACTPRI, ActionPriority = new Attribute(LABEL.ACTPRI, 10, this));
                    _type = "INVALID";
                    break;
            }
            // once base attributes are done, init bot based on type / etc
            // DO not need to update the bot here, just the teck level
            MyLogger.Log(_name + " " + _type + " created for user " + _player);
            // MyLogger.Log("Updating for " + _name);           
            UpdateTechLevel();
        }

        // --- PRIVATE METHODS

        // Revert all CURRENT values back to their RAW value
        public void UpdateEntity()
        {
            // Update the tech level            
            UpdateTechLevel();
            foreach (KeyValuePair<LABEL, Attribute> p in AttributeDict)
            {
                p.Value.Update(this);                         
            }

            // TEMP
            AttachedGear.Add(new Gear());

            foreach (Gear g in AttachedGear)
            {
                g.Update();
            }

            PopulateAttributes();
            MyLogger.Log(_name + " updated.");
            DataManager.SaveObjToFile(_id.ToString(), this);
        }

        private void UpdateTechLevel()
        {
            _techLvl = Off.Total + Def.Total + Eff.Total + Acc.Total + 20;
        }

        // Update ALL total/current values, based on RAW and Bonus
        
        public void PopulateAttributes()
        {
            foreach (KeyValuePair<LABEL, Attribute> pair in AttributeDict)
            {
                switch (pair.Key)
                {
                    case LABEL.OFF:
                        Off = pair.Value;
                        break;
                    case LABEL.DEF:
                        Def = pair.Value;
                        break;
                    case LABEL.EFF:
                        Eff = pair.Value;
                        break;
                    case LABEL.ACC:
                        Acc = pair.Value;
                        break;
                    case LABEL.INTEG:
                        Integrity = pair.Value;
                        break;
                    case LABEL.DMGBNS:
                        DamageBonus = pair.Value;
                        break;
                    case LABEL.DMGRES:
                        DamageResist = pair.Value;
                        break;
                    case LABEL.HITCHN:
                        HitChance = pair.Value;
                        break;
                    case LABEL.CRICHN:
                        CritChance = pair.Value;
                        break;
                    case LABEL.EVACHN:
                        EvadeChance = pair.Value;
                        break;
                    case LABEL.ACTPRI:
                        ActionPriority = pair.Value;
                        break;
                    default:
                        MyLogger.Log("ATTEMPT TO WRITE TO INVALID ATTRIBUTE VALUE");
                        break;
                }
            }
        }    
               
        public void OutputAttributes()
        {
            Console.WriteLine(Off.GetSpread());
            Console.WriteLine(Def.GetSpread());
            Console.WriteLine(Eff.GetSpread());
            Console.WriteLine(Acc.GetSpread());
            Console.WriteLine(Integrity.GetSpread());
            Console.WriteLine(DamageBonus.GetSpread());
            Console.WriteLine(DamageResist.GetSpread());
            Console.WriteLine(HitChance.GetSpread());
            Console.WriteLine(CritChance.GetSpread());
            Console.WriteLine(EvadeChance.GetSpread());
            Console.WriteLine(ActionPriority.GetSpread());
        }

        public void AddWin()
        {
            _wins++;
        }

        public void AddLoss()
        {
            _losses++;
        }

        public string GetName()
        {
            return _name;
        }

        public ulong GetId()
        {
            return _id;
        }
        
        // output the stats of the bot in a string
        public string DisplayScore()
        {
            //-[Bot Owner] [Bot Name] - - - - - - - - - - - - - - - -
            //    [Bot Type] - Tech Level : 9999
            //    [HULL: 999 | CORE: 999 | MOBI: 999 | OPTI: 999 ]
            //- - - - - - - - - - - - - - - - - - - - - - - - - - - -
            string line1 = "[ Handler: " + _player + " ] [ BotID: " + _name + " ] [ Integrity: " + Integrity.Current + "/" + Integrity.Total + " ]\n";
            string line2 = "\tInfo -> [ " + _type + " ] [ W - " + _wins + "/ L -" +  _losses + " ] - Tech Level : " + _techLvl + "\n";
            string line3 = "\tStats -> [ OFFENSE: " + Off.Total + " | DEFFENSE: " + Def.Total + " | EFFECIANCY: " + Eff.Total + " | ACCURACY: " + Acc.Total + " ]\n";
            // foreach (KeyValuePair<LABEL, Attribute> p in AttributeDict ) { Console.Write(p.Key + "\t"); Console.WriteLine("\t" + p.Value.GetLabel() + "\n"); }
            return line1 + line2 + line3;
        }
    }
}
