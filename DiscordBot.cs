﻿using Discord;
using Discord.Commands;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatBattlers
{
    class DiscordBot
    {
        // -- PRIVATE MEMBERS

        private Game _mainGame = new Game();
        private DiscordClient _client;
        // public int commandCoolDown = 30; // should be in seconds
        // public bool fightState = false; // one fight at a time?
        private string _token = "MjY2MzY3NzcyOTE5OTg4MjI1.C08tPA.ME1tXtLdrf06P1zeWK6R3Bk7BbE";

        // -- CONSTRUCTORS - DESTRUCTORS        
        public DiscordBot()
        {
            _client = new DiscordClient(x =>
            {
                x.AppName = "Battle Bot Manager";
                x.AppUrl = "http://url.net";
                x.LogLevel = LogSeverity.Verbose;
                x.LogHandler = LogDiscord;
            });

            _client.UsingCommands(x =>
            {
                x.PrefixChar = '>';
                x.AllowMentionPrefix = true;
                x.HelpMode = HelpMode.Public;
            });

            // Create all useable commands
            MakeCommands();

            _client.ExecuteAndWait(async () =>
            {
                MyLogger.Log("Running Bot");
                await _client.Connect(_token, TokenType.Bot);
            });
        }

        // -- PRIVATE METHODS
        private void LogDiscord(Object sender, LogMessageEventArgs e)
        {
            Console.WriteLine($"[{e.Severity}] [{e.Source}] {e.Message}");
            // [INFO] [DISCORD] msg
        }        

        // -- PUBLIC METHODS
        public void MakeCommands()
        {
            var cServ = _client.GetService<CommandService>();

            /*
             *  We want to ONLY make calls to functions within the Game object
             *  Keep the command code bodys as small and compact as possible, and abstract everything we can to the Game object
             *  Should only be a single AWAIT statement in each command, at the end, to output our intended result.
             */

           // Register a new user.
            cServ.CreateCommand("register")
                .Description("Register as a new Player!\n The <name> is the name of your ChatBattler.\nThe <type> must be one of the following; 'SCOUT', 'ASSAULT', 'HEAVY'.")
                .Parameter("entName", ParameterType.Required)
                .Parameter("entType", ParameterType.Required)
                .Do(async (e) =>
                {
                    // setup input vars
                    var toReturn = $"";
                    var entityName = e.GetArg("entName");
                    var entityType = e.GetArg("entType");

                    // add new user to mainGame
                    bool success = _mainGame.NewUser(e.User.Id, e.User.Name, entityName, entityType);

                    // display output.
                    if (success)
                    {
                        toReturn = $"Welcome to battlebotz @{e.User.Name}.";
                    }
                    else
                    {
                        toReturn = $"Looks like you are already registered @{e.User.Name}.";
                    }

                    // output result
                    await e.Channel.SendMessage(toReturn);
                });

            // Delete the info of the user calling it. 
            cServ.CreateCommand("deregister")
                .Description("Remove yourself from the list active players! WARNING: This WILL DELETE all of your progress!")
                .Do(async (e) =>
                {
                    var toReturn = $"";
                    bool error = _mainGame.RemUser(e.User.Id);
                    _mainGame.RemUser(e.User.Id);
                    // display output.

                    if (!error)
                    {
                        toReturn = $"Godobye @{e.User.Name}.";
                    }
                    else
                    {
                        toReturn = $"Could not find any user for @{e.User.Name}.";
                    }

                    await e.Channel.SendMessage(toReturn);
                });

            // List all registered users.
            cServ.CreateCommand("list")
                .Description("Who is playing?")
                .Do(async (e) =>
                {
                    await e.Channel.SendMessage(_mainGame.ListUsers());
                });

           // Check target/own Battler stats.
            cServ.CreateCommand("check")
               .Description("Check your bot's stats, you can also check the stats of other folks bots by mentioning them! Example: '>check @target'")
               .Parameter("target", ParameterType.Required)
               .Do(async (e) =>
               {                   
                      
                   Player player = _mainGame.GetPlayer(e.Message.MentionedUsers.First().Id);
                                     
                   var toReturn = $"";
                   if (player != null)
                   {
                       Entity entity = _mainGame.GetEntity(player.GetId());
                       Console.WriteLine("Get entity.");
                       if (entity != null)
                       {
                           Console.WriteLine(entity.DisplayScore());
                           toReturn = entity.DisplayScore();
                       }
                       else
                       {
                           toReturn = $"They do not have a current ChatBattler!";
                       }
                   }
                   else
                   {
                       toReturn = $"@{e.Message.MentionedUsers.First().Name} is not a user.";
                   }
                   await e.Channel.SendMessage(toReturn);
               });

            // Update / Revert / Save the data of who invokes the command.
            cServ.CreateCommand("save")
               .Description("Save user progress.")
               .Do(async (e) =>
               {
                   Player player = _mainGame.GetPlayer(e.User.Id);
                   var toReturn = $"";

                   if (player != null)
                   {
                       _mainGame.GetEntity(player.GetId()).UpdateEntity();
                       player.UpdatePlayer();
                       toReturn = $"@{player.GetName()} is now updated.";
                   }
                   else
                   {
                       toReturn = $"@{e.User.Id} is not a user.";
                   }
                   await e.Channel.SendMessage(toReturn);
               });

            // Repair / Revert the Integrity of the invokers Battler.
            cServ.CreateCommand("repair")
              .Description("Repair your chat battler.")
              .Do(async (e) =>
              {
                  Entity entity = _mainGame.GetEntity(e.User.Id);
                  var toReturn = $"";

                  if (entity != null)
                  {
                      entity.Integrity.Revert();
                      toReturn = $"@{entity.GetName()} is now repaired!";
                  }
                  else
                  {
                      toReturn = $"@{e.User.Id} is not a entity!.";
                  }
                  await e.Channel.SendMessage(toReturn);
              });
                       
            // Fight target players Battler
            cServ.CreateCommand("fight")
                .Description("Fight the target bot! Simply mention the target! '>fight @target'")
                .Parameter("target", ParameterType.Required)
                .Do(async (e) =>
                {
                    if (e.User.Id != e.Message.MentionedUsers.First().Id)
                    {
                        await e.Channel.SendMessage(_mainGame.FightResult(e.User.Id, e.Message.MentionedUsers.First().Id, e));
                    }
                    else
                    {
                        await e.Channel.SendMessage("You can not fight yourself!");
                    }
                });

        }
    }
}
