﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//include
using System.IO;
using Discord;
using Discord.Commands;
using System.Threading;

// Game tracks all the data in the game itself
// the players for example and links them to there bots
//  - players have inventories, and stats win/loss, etc 
//  - bots have stats, slots, and a tech_lvl
// game saves the data once every interval
// game can load data on a app restart

namespace ChatBattlers
{
    class Game
    {
        // -- PRIVATE MEMBER VARIABLES --

        // List of all the users playing the game, keyed by a ulong userid, given by discord
        static private Dictionary<ulong, Player> _playerData;
        static private Dictionary<ulong, Entity> _entityData;
        static private Dictionary<uint, Gear> _gearData;
        static private Dictionary<uint, Tech> _techData;        
        GameTimer gameTimer;

        // -- CONSTRUCTORS / DESTRUCTORS --

        public Game()
        {
           
            MyLogger.Log("Setting Up Game");

            gameTimer = new GameTimer();        

            // init gameData dicts
            _playerData = new Dictionary<ulong, Player>();
            _entityData = new Dictionary<ulong, Entity>();
            _gearData = new Dictionary<uint, Gear>();
            _techData = new Dictionary<uint, Tech>();     

            // Load data
            Console.WriteLine("Preping to load data from dirs.\n");
            string[] playerDirs = Directory.GetDirectories(DataManager.PlayerPathExt);
            foreach (string s in playerDirs) { Console.WriteLine(s + "\\"); }

            // Temp storage for file paths
            string[] playerData;
            string[] entityData;
            string[] gearData;

            // get all players
            Console.WriteLine("Loading Player Data.");
            Console.WriteLine(playerDirs.Count() + " found.");
            for (int i = 0; i < playerDirs.Count(); i++)
            {
                Console.WriteLine("Trying to access dir: ");
                Console.WriteLine(playerDirs[i] + "\\\n");
                // get all files in current dir
                playerData = Directory.GetFiles(playerDirs[i] + "\\", "*_p.*");
                foreach (string s in playerData) { Console.WriteLine(s + "\n"); }
                for (int j = 0; j < playerData.Count(); j++)
                {
                    Console.WriteLine("First Call To Constructor");
                    Player player;

                    Console.WriteLine("Second Call");
                    player = DataManager.LoadPlayerFromFile(playerDirs[i] + "\\", playerData[j]);
                    _playerData.Add(player.GetId(), player);
                    MyLogger.Log("[ " + player.GetId() + " ] id loaded for user " + player.GetName());
                }
            }

            // get all the entitys
            Console.WriteLine("\nLoading Entity Data.");
            for (int i = 0; i < playerDirs.Count(); i++)
            {
                Console.WriteLine("Trying to access dir: ");
                Console.WriteLine(playerDirs[i] + "\\\n");
                // get all files in current dir
                entityData = Directory.GetFiles(playerDirs[i] + "\\", "*_e.*");
                foreach (string s in entityData) { Console.WriteLine(s + "\n"); }
                for (int j = 0; j < entityData.Count(); j++)
                {
                    Entity entity;
                    entity = DataManager.LoadEntityFromFile(playerDirs[i], entityData[j]);
                 
                    // For this entity, grab is Att data
                    string playerHome = DataManager.PlayerPathExt + entity.GetId() + "\\";
                    string entityAttributes = playerHome + "Attributes\\";
                    string[] attrFiles = Directory.GetFiles(entityAttributes);
                    // For each Attribute File found in dir Attributes, load the file, and att it to the entity       
                    for (int k = 0; k < attrFiles.Count(); k++)
                    {
                        Attribute a = DataManager.LoadAttributeFromFile(attrFiles[k]);
                        entity.AttributeDict.Add(a.Label, a);                              
                    }
                    // Finally add our loaded entity to the list
                    _entityData.Add(entity.GetId(), entity);
                    MyLogger.Log("[ " + entity.GetId() + " ] id loaded for " + entity.GetName());                    
                    GetEntity(entity.GetId()).PopulateAttributes();
                    // MyLogger.Log();
                }
            }

            // get all the gear!
            Console.WriteLine("\nLoading Gear Data.");
            Console.WriteLine("Trying to access dir: \n" + DataManager.GearPathExt);
            // get all files in current dir
            gearData = Directory.GetFiles(DataManager.GearPathExt);
            foreach (string s in gearData) { Console.WriteLine(s + "\n"); }
            for (int i = 0; i < gearData.Count(); i++)
            {
                Gear g = DataManager.LoadGearFromFile(gearData[i]);
                _gearData.Add(g.Gid, g);
            }

        }

        // -- PRIVATE --      

        private ulong? SimulateCombat(ulong offense, ulong defense, CommandEventArgs e)
        {
           
            Entity off = GetEntity(offense);
            Entity def = GetEntity(defense);

            // Combat consists of three rounds.
            // Winner decided by lowest Integrity after last round or, one combatant reaches zero intregrit
            // For X rounds, do CombatRound
            for (int i = 1; i <= 3; i++)
            {
                CombatRound(ref off, ref def, e);
                if (def.Integrity.Current <= 0 || (def.Integrity.Current < off.Integrity.Current && i == 3))
                {
                    off.AddWin();
                    def.AddLoss();
                    return offense;
                }
                else if (off.Integrity.Current <= 0 || (off.Integrity.Current < def.Integrity.Current && i == 3))
                {
                    off.AddLoss();
                    def.AddWin();
                    return defense;
                }
                gameTimer.Delay(2);
            }
            off.UpdateEntity();
            def.UpdateEntity();
            return null; // null for no victor
        }

        private void CombatRound(ref Entity off, ref Entity def, CommandEventArgs e)
        {
            string roundResult;         
            
            e.Channel.SendMessage("################## !NEXT ROUND! ##################");

            ulong first;
            Console.WriteLine("Getting Priority!");

            if (off.ActionPriority.Total < def.ActionPriority.Total)
            {
                first = def.GetId();
            }
            else if (off.ActionPriority.Total > def.ActionPriority.Total)
            {
                first = off.GetId();
            }
            else
            {
                first = off.GetId();
            }

            // if off is first, do attack
            int dmg;
            if (off.GetId() == first)
            {
                dmg = 0;
                dmg = CombatDoAttack(off, def);
                if (dmg > 0)
                {
                    def.Integrity.Current -= dmg;
                    roundResult = "HIT! | " + off.GetName() + " deals a total of " + dmg + "! Leaving " + def.GetName() + " with only " + def.Integrity.Current + "/" + def.Integrity.Total + " Integretiy!";
                                         
                }
                else
                {
                    roundResult = "MISS!";
                }
                e.Channel.SendMessage(roundResult);

                dmg = 0;
                dmg = CombatDoAttack(def, off);
                if (dmg > 0)
                {
                    off.Integrity.Current -= dmg;
                    roundResult = "HIT! | " + def.GetName() + " deals a total of " + dmg + "! Leaving " + off.GetName() + " with only " + off.Integrity.Current + "/" + off.Integrity.Total + " Integretiy!";
                    
                }
                else
                {
                    roundResult = "MISS!";
                }
                e.Channel.SendMessage(roundResult);
            }
            else
            {
                dmg = 0;
                dmg = CombatDoAttack(def, off);
                if (dmg > 0)
                {
                    off.Integrity.Current -= dmg;
                    roundResult = "HIT! | " + def.GetName() + " deals a total of " + dmg + "! Leaving " + off.GetName() + " with only " + off.Integrity.Current + "/" + off.Integrity.Total + " Integretiy!";
                   
                }
                else
                {
                    roundResult = "MISS!";
                }
                e.Channel.SendMessage(roundResult);

                dmg = 0;
                dmg = CombatDoAttack(off, def);
                if (dmg > 0)
                {
                    def.Integrity.Current -= dmg;
                    roundResult = "HIT! | " + off.GetName() + " deals a total of " + dmg + "! Leaving " + def.GetName() + " with only " + def.Integrity.Current + "/" + def.Integrity.Total + " Integretiy!";
                    
                }
                else
                {
                    roundResult = "MISS!";
                }
                e.Channel.SendMessage(roundResult);
            }
          
        }

        private int CombatDoAttack(Entity off, Entity def)
        {
            Console.WriteLine("Doing Attack Calcs");
            // calc hit chance
            Random hitDie = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);
            Thread.Sleep(5);
            Random evadeDie = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);
            Thread.Sleep(5);
            Random critDie = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);
            Thread.Sleep(5);
            Random dmgDie = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);

            bool hitCheck = false;
            bool evaCheck = false;
            bool critCheck = false;
            int hitRoll = 0;
            int evadeRoll = 0;
            int critRoll = 0;
            int dmgRoll = 0;

            // Roll higher than Total for success.
            // 70 Total HitChance means I need to roll 70 OR LESS.
            hitRoll = hitDie.Next(100) + 1;            
            if (off.HitChance.Total > hitRoll)
            {
                hitCheck = true;
            }

            // Evade Chance of 9, means I must ROLL 9 OR LESS.
            evadeRoll = evadeDie.Next(100) + 1;
            if (off.EvadeChance.Total > evadeRoll)
            {
                evaCheck = true;
            }
            

            // Off hit, and Def did not not evade
            if (hitCheck && !evaCheck)
            {
                // if we crit
                critRoll = critDie.Next(100) + 1;
                if (off.CritChance.Total > critRoll)
                {
                    critCheck = true;
                }
                // Build Damage Total, 
                // 40 / 4 = 10 + dmgBns
                // 40 / 2 = 20 + dmgBns
                int minDmg = ((off.Off.Total / 4) + off.DamageBonus.Total);
                int maxDmg = ((off.Off.Total / 2) + off.DamageBonus.Total);

                dmgRoll = dmgDie.Next(minDmg, maxDmg) + 1;
                if (critCheck)
                {
                    dmgRoll *= 2;
                }
                int finalDmg = dmgRoll - def.DamageResist.Total;
                return finalDmg;
            }
            return 0;
        }
            
        // -- PUBLIC --
        
        public bool NewUser(ulong id, string userName, string entityName, string entityType) {
            if (GetPlayer(id) == null)
            {
                DataManager.MakeNewPlayerDir(id.ToString());

                Console.WriteLine("Create player object");
                Player player = new Player(id, userName);

                Console.WriteLine("Create entity object");
                Entity entity = new Entity(id, userName, entityName, entityType);

                DataManager.SaveObjToFile(id.ToString(), player);
                DataManager.SaveObjToFile(id.ToString(), entity);

                _playerData.Add(id, player);
                _entityData.Add(id, entity);
                return true;
            }
            return false;                        
        }

        public bool RemUser(ulong id) {
            Player player = GetPlayer(id);
            if (player != null)
            {
                MyLogger.Log(player.GetName() + " " + player.ToString() + " removed.");
                _playerData.Remove(id);
                _entityData.Remove(id);
                DataManager.RemovePlayerDir(id.ToString());            
                return true;
            }
            return false;
        }
        
        public string ListUsers()
        {
            string msg = "Players: \n";
            foreach (KeyValuePair<ulong, Player> player in _playerData)
            {
                Entity entity;
                _entityData.TryGetValue(player.Value.GetId(), out entity);
                msg += "\t[ User: " + player.Value.GetName() + " | Battler: " + entity.GetName() + "! ]\n";
                
            }
            MyLogger.Log(msg);
            return msg;            
        }
        
        public string FightResult(ulong id, ulong target, CommandEventArgs e)
        {
            MyLogger.Log("[ " + id + " ] Attacker vs [ " + target + " ] Deffender");
            ulong? winner = SimulateCombat(id, target, e);
            if (winner == null)
            {
                // Techincally should never happen.
                return "There was no victor.";
            }
            return GetPlayer((ulong)winner).GetName() + " is the victor!\n";           
        }
        
        public Player GetPlayer(ulong id)
        {
            Player player;
            if (_playerData.TryGetValue(id, out player))
            {
                MyLogger.Log("User found with id [" + id + "] name [" + player.GetName() + "]");
                return player;
            }
            return null;
        }

        public Entity GetEntity(ulong id)
        {
            Entity entity;
            if (_entityData.TryGetValue(id, out entity))
            {
                MyLogger.Log("Entity found with id [" + id + "] name [" + entity.GetName() + "]");
                return entity;
            }
            return null;
        }

    }
}
