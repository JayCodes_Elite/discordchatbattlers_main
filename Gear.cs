﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace ChatBattlers
{

    class Gear
    {
        [JsonProperty]
        public string Name = "INVALID";
        [JsonProperty]
        public string GType; // defined in json
        [JsonProperty]
        public uint Gid; // defined in json

        [JsonProperty]
        public int Tier;
        [JsonIgnore]
        public static int GearCount;

        [JsonProperty]
        public int OffMod = 0;
        [JsonProperty]
        public int DefMod = 0;
        [JsonProperty]
        public int EffMod = 0;
        [JsonProperty]
        public int AccMod = 0;

        // -- CONSTRUCTORS

        [JsonConstructor]
        public Gear()
        {

        }

        public Gear(string name)
        {
            GearCount++;
            Name = name;
            Console.WriteLine("Count is: " + GearCount);
            if (GearCount > 30)
            {
                genMods(5);
            }
            else if (GearCount > 15)
            {
                genMods(4);
            }
            else if (GearCount > 5)
            {
                genMods(3);
            }
            else
            {
                genMods(2); // 5
            }

            setTier(); // set tier
            Console.WriteLine("Tier: " + Tier);
            genGid();  // gen the id
            Console.WriteLine("Gid: " + Gid);
        }

        // -- PRIVATE METHOD       
        private void setTier()
        {
            // -10, 10, 20, 0 = 20 / 10 = 2 = TIER 2
            Tier = (OffMod + DefMod + EffMod + AccMod) / 10;
        }

        private void genMods(int tier)
        {

            // how many pos to apply            

            for (int i = 0; i < tier; i++)
            {
                Random rand1 = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);
                int r1 = rand1.Next(1, 5);
                Console.WriteLine("RAND TO POS: " + r1);
                switch (r1)
                {
                    case 1:
                        OffMod += 10;
                        break;
                    // case
                    case 2:
                        DefMod += 10;
                        break;
                    //case
                    case 3:
                        EffMod += 10;
                        break;
                    //case
                    case 4:
                        AccMod += 10;
                        break;
                    //case
                    default:
                        Console.WriteLine("oops");
                        break;
                        // oops
                }
            }

            int negs = 0;

            if (tier == 5 || tier == 4)
            {
                negs = 2;
            }
            else if (tier == 3 || tier == 2)
            {
                negs = 1;
            }

            for (int i = 0; i < negs; i++)
            {
                Random rand1 = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);
                int r1 = rand1.Next(1, 5);
                Console.WriteLine("RAND TO NEG: " + r1);
                switch (r1)
                {
                    case 1:
                        OffMod -= 10;
                        break;
                    // case
                    case 2:
                        DefMod -= 10;
                        break;
                    //case
                    case 3:
                        EffMod -= 10;
                        break;
                    //case
                    case 4:
                        AccMod -= 10;
                        break;
                    //case
                    default:
                        Console.WriteLine("oops");
                        break;
                        // oops
                }
            }

            int max = Math.Max(Math.Max(OffMod, DefMod), Math.Max(EffMod, AccMod));
            if (max == OffMod)
            {
                GType = "AGGRO";
            }
            else if (max == DefMod)
            {
                GType = "WALL";
            }
            else if (max == EffMod)
            {
                GType = "LOGIC";
            }
            else if (max == AccMod)
            {
                GType = "OPTIC";
            }
            else
            {
                GType = "GENERAL";
            }

        }

        private void genGid()
        {
            Random rand = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);
            // 1-5 + (1000-9999) + gearCount
            // 1,000,000 - 5999999
            Gid = ((uint)Tier * 10000000) + (uint)rand.Next(1000, 10000) * 100000 + (uint)GearCount;
        }

        // -- PUBLIC METHODS

        // TODO-- Decouple Entity Argument, from apply and remove

        public void Update()
        {
            // gear_thisgear_3.json
            DataManager.SaveObjToFile(null, this);
        }

    }
}
