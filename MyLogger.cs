﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatBattlers
{
    static class MyLogger
    {            
        public enum LEVEL
        {
            TRACE,
            INFO,
            DEBUG
        }

        static private DateTime startTime = DateTime.Now;

        public static void Log(string msg, LEVEL level = LEVEL.INFO)
        {
            string msgFixed;
            string severity;

            switch (level)
            {
                case LEVEL.TRACE:
                    severity = "[Trace] ";
                    break;
                case LEVEL.INFO:
                    severity = "[Info] ";
                    break;
                case LEVEL.DEBUG:
                    severity = "[Debug] ";
                    break;
                default:
                    severity = "[Error] ";
                break;
            }
            msgFixed = "[MyLog] " + severity + msg;
            Console.WriteLine(msgFixed);
            //log output example
            // [LogLevel] : [Msg]
        }
    }
}
