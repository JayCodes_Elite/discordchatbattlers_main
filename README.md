# README #

### What is this repository for? ###

* Discord Chat Battlers:
A PVP Strategy / Fighting game for discord users.
* Version:
No versioning system implemented yet

### How can I help? ###

* Watch The Live Stream!

First and foremost you can watch the live stream! I stream almost 80% of the development of the project live at: https://www.liveedu.tv/jaycodes/

* Join the discord!

Live tests are welcome! Join the discord chat and get in on the action! https://discord.gg/XDfRB7C

### Contribution guidelines ###

* Want to contribute to the code! 

Check the wiki for [How to Join] to request a invitation as a developer. There is allot of needed features and plenty of work to go around! Although this is a bit of a dictatorship-formatted project, input is always welcome.

* Code review Me!

This will help me personally grow as I am always looking for input on my code, please feel free to comment on styling, syntax, formatting, design, and logic! I am always looking to refactor, and grow as a developer and good solid good reviews help this! Code reviews can be sent to me via email: jasonalanterry@gmail.com           

### Who do I talk to? ###

* Project Lead: Jason Terry @ jasonalanterry@gmail.com