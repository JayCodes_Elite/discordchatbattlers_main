﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ChatBattlers
{

    enum LABEL
    {
        INVALID = 0, 
        OFF,
        DEF,
        EFF,
        ACC,
        INTEG,
        DMGBNS,
        DMGRES,
        HITCHN,
        CRICHN,
        EVACHN,
        ACTPRI,
        TECLVL
    }

    class Attribute
    {

        [JsonProperty]
        public LABEL Label;
        [JsonProperty]
        public int Raw; // The base value to be unchanged
        [JsonProperty]
        public int Bonus; // temp value, used to hold changes during combat
        [JsonProperty]
        public int Current; // 
        [JsonProperty]
        public int Total; // value to display

        // -- CONSTRUCTOR --

        [JsonConstructor]
        public Attribute(LABEL label = 0, int initVal = 0)
        {
            // Load from JSON
            Label = label;
            Raw = initVal;
            Bonus = 0;
            Console.WriteLine("Loading attribute " + GetLabel() + " from JSON, in constructor");
        }

        public Attribute(LABEL label, int initVal, Entity e)
        {
            Label = label;
            Raw = initVal;             
            Bonus = 0;
            Update(e); // Total to Bonus + Raw
            Revert(); // set current
        }
        
        // -- PRIVATE --     

        // Update Total value with appropriate formula
        private void UpdateCoreAttribute()
        {
            Total = Raw + Bonus;
        }

        private void UpdateIntegrity(Entity e)
        {
            // SCOUT = 25, ASSUALT = 50, HEAVY = 75
            // Formula: base + BONUS + DEFFENSE(25%) + EFFICENCY(25%)
            // Example: base(50) + BONUS + 20(5) + 20(5) = 60
            Total = Raw + Bonus + GetPrecentage(e.Def.Total, 25) + GetPrecentage(e.Eff.Total, 25);

        }

        private void UpdateDamageBonus(Entity e)
        {
            // SCOUT = 15, ASSUALT = 10, HEAVY = 5
            // Formula: base + OFFENSE(10%)
            // Example: base(15) + 25(2.5) = 17.5
            Total = Raw + GetPrecentage(e.Off.Total, 10);
        }

        private void UpdateDamageResist(Entity e)
        {
            // SCOUT = 5, ASSUALT = 10, HEAVY = 15
            // Formula: base + DEFFENSE(10%)
            // Example: base(15) + 25(2.5) = 17.5
            Total = Raw + GetPrecentage(e.Def.Total, 10);
        }

        private void UpdateHitChance(Entity e)
        {
            // SCOUT = 65, ASSUALT = 60, HEAVY = 50
            // Formula: base + acc(50 %)
            // Formula: base(60) + 15(7) = 67 / 100
            Total = Raw + 20 + GetPrecentage(e.Acc.Total, 10);
        }

        private void UpdateCritChance(Entity e)
        {
            // SCOUT = 4, ASSUALT = 8, HEAVY = 2
            // Formula: base + ACCUARCY(10%)
            // Example: base(8) + 20(2) = 10/100
            Total = Raw + GetPrecentage(e.Acc.Total, 10);
        }

        private void UpdateEvadeChance(Entity e)
        {
            // SCOUT = 8, ASSUALT = 4, HEAVY = 2
            // Formula: base + EFFICIENCY(10%)
            // Example: base(2) + 30(3) = 5/100
           Total = Raw + GetPrecentage(e.Eff.Total, 10);
        }

        private void UpdateActionPriortity(Entity e)
        {
            // Has a base value of 10
            // Formula: base + EFFICIENCY(50%)
            // Example: base(10) + 20(10) = 20
            Total = Raw + GetPrecentage(e.Eff.Total, 50);
        }

        // Returns a value equal to the requested percentage of the given number
        private int GetPrecentage(int val, int p)
        {
            int result;
            float percentage = (p / 100) * val;
            return result = (int) Math.Floor(percentage);             
        }

        // --- PUBLIC METHODS

        // Updates and Saves entity data
        public void Update(Entity e)
        {
            // switch to proper update formula
            // sets the total val for a attribute
            switch (Label)
            {
                case LABEL.OFF:
                    UpdateCoreAttribute();
                    break;
                case LABEL.DEF:
                    UpdateCoreAttribute();
                    break;
                case LABEL.EFF:
                    UpdateCoreAttribute();
                    break;
                case LABEL.ACC:
                    UpdateCoreAttribute();
                    break;
                case LABEL.INTEG:
                    UpdateIntegrity(e);
                    break;
                case LABEL.DMGBNS:
                    UpdateDamageBonus(e);
                    break;
                case LABEL.DMGRES:
                    UpdateDamageResist(e);
                    break;
                case LABEL.HITCHN:
                    UpdateHitChance(e);
                    break;
                case LABEL.CRICHN:
                    UpdateCritChance(e);
                    break;
                case LABEL.EVACHN:
                    UpdateEvadeChance(e);
                    break;
                case LABEL.ACTPRI:
                    UpdateActionPriortity(e);
                    break;
                default:
                    MyLogger.Log("No valid attribute to update", MyLogger.LEVEL.DEBUG);
                    break;
            }
            DataManager.SaveObjToFile(e.GetId().ToString(), this);
        }

        // Reverts Current value to the last updated Total value
        public void Revert()
        {
            Current = Total;
        }

        // Return spread for attribute as a formatted String
        public string GetSpread()
        {
            return "Spread: " + Label + " | [" + Raw + "][" + Bonus + "][" + Current + "][" + Total + "]"; 
        }

        // Get's the label of this attribute as a string
        public string GetLabel()
        {
            switch (Label)
            {
                case LABEL.OFF:
                    return "Offense";                    
                case LABEL.DEF:                    
                    return "Deffense";
                case LABEL.EFF:                    
                    return "Efficency";
                case LABEL.ACC:                    
                    return "Accuracy";
                case LABEL.INTEG:                   
                    return "Integrity";
                case LABEL.DMGBNS:                    
                    return "DamageBonus";
                case LABEL.DMGRES:                  
                    return "DamageResist";
                case LABEL.HITCHN:                    
                    return "HitChance";
                case LABEL.CRICHN:                   
                    return "CritChance";
                case LABEL.EVACHN:                    
                    return "EvadeChance";
                case LABEL.ACTPRI:                    
                    return "ActionPriority";
                default:
                    MyLogger.Log("INVALID LABEL", MyLogger.LEVEL.DEBUG);
                    return "INVALID";
            }
        }        

        // Buff this attribute by one stage
        public void BuffAttribute()
        {
            switch (Label)
            {
                case LABEL.OFF:
                    Bonus += 10;
                    break;
                case LABEL.DEF:
                    Bonus += 10;
                    break;
                case LABEL.EFF:
                    Bonus += 10;
                    break;
                case LABEL.ACC:
                    Bonus += 10;
                    break;
                case LABEL.INTEG:
                    Bonus += 25;
                    break;
                case LABEL.DMGBNS:
                    Bonus += 5;
                    break;
                case LABEL.DMGRES:
                    Bonus += 5;
                    break;
                case LABEL.HITCHN:
                    Bonus += 10;
                    break;
                case LABEL.CRICHN:
                    Bonus += 10;
                    break;
                case LABEL.EVACHN:
                    Bonus += 10;
                    break;
                case LABEL.ACTPRI:
                    Bonus += 5;
                    break;
                default:
                    MyLogger.Log("No attribute buffed");
                    break;
            }
        }

        // Lowers this attribute by a stage
        public void DeBuffAttribute()
        {
            switch (Label)
            {
                case LABEL.OFF:
                    Bonus -= 10;
                    break;
                case LABEL.DEF:
                    Bonus -= 10;
                    break;
                case LABEL.EFF:
                    Bonus -= 10;
                    break;
                case LABEL.ACC:
                    Bonus -= 10;
                    break;
                case LABEL.INTEG:
                    Bonus -= 25;
                    break;
                case LABEL.DMGBNS:
                    Bonus -= 5;
                    break;
                case LABEL.DMGRES:
                    Bonus -= 5;
                    break;
                case LABEL.HITCHN:
                    Bonus -= 10;
                    break;
                case LABEL.CRICHN:
                    Bonus -= 10;
                    break;
                case LABEL.EVACHN:
                    Bonus -= 10;
                    break;
                case LABEL.ACTPRI:
                    Bonus -= 5;
                    break;
                default:
                    MyLogger.Log("No attribute debuffed.");
                    break;
            }
        }
    }
}
