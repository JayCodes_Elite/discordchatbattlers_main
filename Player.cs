﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// include
using Newtonsoft.Json;

namespace ChatBattlers
{
    class Player
    {
        // -- PRIVATE MEMBERS

        // JSON Data
        [JsonProperty]
        private string _name;
        [JsonProperty]
        private ulong _id;

        // -- CONSTRUCTOR - DESTRUCTOR
        public Player()
        {
            Console.WriteLine("Loading player " + _name + " from JSON, in constructor");
        }

        public Player(ulong id, string pName)
        {
            _id = id;
            _name = pName;            
        }

        // -- PRIVATE METHODS

        // -- PUBLIC METHODS            

        public void UpdatePlayer()
        {
            DataManager.SaveObjToFile(_id.ToString(), this);
        }

        public string GetName()
        {
            return _name;
        }

        public ulong GetId()
        {
            return _id;
        }
    }
}
