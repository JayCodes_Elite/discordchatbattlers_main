﻿using System;
using System.Timers;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatBattlers
{
    class GameTimer
    {

        private static Timer _timer;
        private double _count;

        public GameTimer()
        {
            _timer = new Timer(1000);
            // Hook up the Elapsed event for the timer. 
            _timer.Elapsed += Tick;
            _timer.AutoReset = true;
            _timer.Enabled = true;
            Console.WriteLine("The application started at {0:HH:mm:ss.fff}", DateTime.Now);
        }

        public void Start()
        {
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
        }

        public void Puase()
        {
            _timer.Enabled = false;
        }

        public void UnPuase()
        {
            _timer.Enabled = true;
        }

        public double GetCount()
        {
            return _count = _timer.Interval;
        }

        public void Tick(object sender, ElapsedEventArgs e)
        {
            _count = e.SignalTime.Second;                        
        }

        // Delay in seconds
        public void Delay(int delay)
        {
            _timer.Start();
            double initCount = _count;
            while (delay >= _count - initCount)
            {
                // wait
            }
            _timer.Stop();
        }
    }
}
