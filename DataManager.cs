﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//include
using Newtonsoft.Json;
using System.IO;

namespace ChatBattlers
{
    static class DataManager
    {
        public static readonly string Root = AppDomain.CurrentDomain.BaseDirectory;
        public static readonly string PlayerPathExt = Root + "GameData\\PlayerData\\";
        public static readonly string GearPathExt = Root + "GameData\\GearData\\";
        public static readonly string ActionPathExt = Root + "GameData\\ActionData\\";

        // -- CONSTRUCTOR
        static DataManager()
        {
             // Check for proper file structure here
        }

        // -- PUBLIC METHODS

        static public void MakeNewPlayerDir(string id)
        {
            string playerHome = PlayerPathExt + id + "\\";
            string entityAttributes = playerHome + "Attributes\\";
            Directory.CreateDirectory(playerHome);
            Directory.CreateDirectory(entityAttributes);
            Console.WriteLine("Dir made at \n" + playerHome);
        }

        static public void RemovePlayerDir(string dirId)
        {
            if (Directory.Exists(PlayerPathExt + dirId + "\\"))
            {
                MyLogger.Log("Dir found, removing it.");
                Directory.Delete(PlayerPathExt + dirId + "\\", true);               
            }
            else
                MyLogger.Log("Could not delete dir, dir does not exist.");
        }

        static public void SaveObjToFile(string id, Object obj)
        {
            string loc = "";
            string playerHome = PlayerPathExt + id + "\\";
            string entityAttributes = playerHome + "Attributes\\";

            if (obj.GetType().ToString() == "ChatBattlers.Entity")
            {
                Entity e = (Entity) obj;
                loc = Path.Combine(playerHome, id + "_e.json");
            }
            else if (obj.GetType().ToString() == "ChatBattlers.Player")
            {
                Player p = (Player) obj;
                loc = Path.Combine(playerHome, id + "_p.json");
            }
            else if (obj.GetType().ToString() == "ChatBattlers.Attribute")
            {
                Attribute a = (Attribute) obj;
                loc = Path.Combine(entityAttributes + id + "_" + a.GetLabel() + ".json");
            }
            else if (obj.GetType().ToString() == "ChatBattlers.Gear")
            {
                Gear g = (Gear)obj;
                loc = Path.Combine("gear_" + g.Gid + "_" + g.GType + "_" + g.Tier + ".json");
            }
            string json = ObjectToJson(obj);
            Console.WriteLine("Saved json to \n\t[" + loc + "]");
            File.WriteAllText(loc, json);
        }

        static public Player LoadPlayerFromFile(string id, string file)
        {
            string loc = Path.Combine(PlayerPathExt + id, file);
            string json = File.ReadAllText(loc);
            return JsonConvert.DeserializeObject<Player>(json);
        }

        static public Entity LoadEntityFromFile(string id, string file)
        {
            string loc = Path.Combine(PlayerPathExt + id, file);
            string json = File.ReadAllText(loc);
            return JsonConvert.DeserializeObject<Entity>(json);
        }

        static public Attribute LoadAttributeFromFile(string file)
        {            
            string loc = Path.Combine(file);
            string json = File.ReadAllText(loc);
            return JsonConvert.DeserializeObject<Attribute>(json);       
        }

        static public Gear LoadGearFromFile(string file)
        {
            string loc = Path.Combine(file);
            string json = File.ReadAllText(loc);
            return JsonConvert.DeserializeObject<Gear>(json);
        }

        static public void DeleteFile(string pId, string fileId)
        {
            if (File.Exists(PlayerPathExt + "\\" + pId + "\\" + fileId + ".json"))
                File.Delete(PlayerPathExt + "\\" + pId + "\\" + fileId + ".json");
            else
                MyLogger.Log("Could not delte file, file does not exist.");
        }

        static private string ObjectToJson(Object obj)
        {
            return JsonConvert.SerializeObject(obj, Formatting.Indented);
        }
        
    }
}

