----- Discord Chat Battlers ---

LIST OF NAMES FOR THE BOTS - Strawpoll
XFrame
Exos
Frame
Cyborgs

GOAL #3.1

Abstract the Attribute struc, into a class, to build the rest
of the Attribute classes from.

--- oversights ---

command overlap / to many people, doing commands, at the same time.
	- cool down period betweens command

------------------------
- Discord Battle Bot - 
Fight Spam Control System

Fight is a created chatroom to output the play by play,
and result of a fight between two [MechName]
- - - - - - - - - - - - - - - - - - - - - -  - - - - - - 
1. Fight request is inputed
2. Fight request sent to FightQue
3. FightQue creates a fightRoom(Discord chat) for the fight
# JayCodes VS IronDragon000
4. The fight plays out
5. output the fight result, save fight data, revert bot states
6. destroy fight room / free 

FightQue
	Contains all requested fights

maxFights = userdefined (5)

FightQue
	Check the number of active fights 


--------------------------------------------------------------
JSON Database Structure

/GameData/
	/gear/
		gearid_type_name.json // all gear data stored in individual json
		gearid_type_name.json // the entity has a list of gear id's
		gearid_type_name.json // this list is used to pull only what it has equiped
		gearid_type_name.json // the player has a list of gear id's
		gearid_type_name.json // which get put into memory as player inventory
	/players/ ?Can I serielize this folder?
		/*playerid*/
			/Gear/

			/Actions/

			/Attributes/
				playerid_attributename.json // attribute data
			playerid_p.json // player data
			playerid_e.json // entity data
		/*playerid*/
			/Gear/
			/Actions/
			/Attributes/
			playerid_p.json
			playerid_e.json
		/*playerid*/
			/Gear/
			/Actions/
			/Attributes/
			playerid_p.json
			playerid_e.json
		/*playerid*/
			/Gear/
			/Actions/
			/Attributes/
			playerid_p.json
			playerid_e.json
app.exe
config.ini
*other game data needed to run app*

---------------------------------------------------------------------------
[Unamed Discord Bot Game Systems]
?Discord.BattleNetwork

[Player]
Players

Defined: The user, who has ownership of a [Bot] within the game scope
-- Has
pid discord user id

[Entity]
!ChatBattler *
!Runtime
?BattlerProtocol 

Defined: Are used by the [Player], to fight against other [Bot]s

-- Has

type : Determines stats at creation, starting [gear], starting [functions],
	Also determines equipable [gear], [functions], and [growth]

	SCOUT		25 / 10 / 20 / 25
		- Focus on high damage and crit chance, low def
	ASSUALT		20 / 20 / 15 / 25
		- Well rounded, most build options
	HEAVY		20 / 30 / 15 / 15
		- Focus on defense, and sust damage

// IGNORE
gear : alters [CoreAttributes]
functions : preform attacks / actions in combat
// IGNORE

abstract Attribute
	raw
	bonus
	total
	current

	Update() - overloaded
	Revert()

	Offense : Attribute
		Update()
	Deffense : Attribute
		Update()
	Efficency : Attribute
		Update()
	Accuracy : Attribute
		Update()
	etc....


[CoreAttribues]
	- (Baseline of 80 total at creation)	
	OFFENSE		{OFF_VAL} 	- Affects damage a bot deals
	DEFFENSE	{DEF_VAL} 	- Affects how much damage a bot can take
	EFFICIENCY	{EFF_VAL} 	- Evasion of a [Bot], affects dodge chance, and turn order
	ACCUARCY	{ACC_VAL} 	- Accuracy of a [Bot], affects chance to hit, and crit chance

[DerivedAttributes]
	- Derived attributes each have independent formulas that generate them

		TECH_LVL 
			Def: Numeric Representation Of a Bots overall power rating
			- Has a base value of 20
			Formula: 20 + raw of all stats combined (baseline of 100 at creation)

		INTEGRITY
			Def: Total damage a bot can take before destruction
			- Has a base value dependent on the [bot] [type]
				SCOUT = 25, ASSUALT = 50, HEAVY = 75
			Formula: base + DEFFENSE(25%) + EFFICENCY(25%)
			Example: base(50) + 20(5) + 20(5) = 60

		DAMAGE_BONUS[HIDDEN]
			Def: Added to function damage in combat.
			- Has a base value dependent on the [bot] [type]
				SCOUT = 15, ASSUALT = 10, HEAVY = 5
			Formula: base + OFFENSE(10%)
			Example: base(15) + 25(2.5) = 17.5

		DAMAGE_RESIST[HIDDEN]
			Def: Reduce damage taken by this amount before being applied.
			- Has a base value dependent on the [bot] [type]
				SCOUT = 5, ASSUALT = 10, HEAVY = 15
			Formula: base + DEFFENSE(10%)
			Example: base(10) + 20(2) = 12

		HIT_CHANCE[HIDDEN?]
			Def: Chance, out of 100, to land a hit when a attack [function] is used
			- Has a base value dependent on the [bot] [type]
				SCOUT = 65, ASSUALT = 60, HEAVY = 50
			Formula: base + acc(50%)
			Formula: base(60) + 15(7) = 67/100

		CRIT_CHANCE[HIDDEN]
			Def: Chance, out of 100, to land a crit when a attack [function] is used.
			- Has a base value dependent on the [bot] [type]
			// SCOUT = 4, ASSUALT = 8, HEAVY = 2
			// Formula: base + ACCUARCY(10%)
			// Example: base(8) + 20(2) = 10/100

		EVADE_CHANCE[HIDDEN]
			Def: Chance, out of 100, to land a crit when a attack [function] is used.
			- Has a base value dependent on the [bot] [type]
			// SCOUT = 8, ASSUALT = 4, HEAVY = 2
			// Formula: base + EFFICANCY(10%)
			// Example: base(2) + 30(3) = 5/100

		ACTION_PRIORITY[HIDDEN]
			Def: Number used to determine turn order in combat, if tied, chosen by 			highest EFF
			// Has a base value of 10
			// Formula: base + EFFICIENCY(50%)
			// Example: base(10) + 20(10) = 20


/// DEFINE TECH

doCombat() {
	// maths
}

e.doNextAction(Entity targ)

doAction() {
	// does diffrent maths...
}



// DEFINE GEAR


























































